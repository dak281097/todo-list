import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToDoListComponent } from './containers/ToDo-List/ToDo-list.component';
import { CompletedTasksComponent } from './components/completed tasks/completed-tasks.component';
import { ActiveTasksComponent } from './components/active-tasks/active-tasks.component';



@NgModule({

    declarations: [
        ToDoListComponent,
        CompletedTasksComponent,
        ActiveTasksComponent
    ],
    imports:[
        CommonModule, 
    ],
    exports:[
        ToDoListComponent,
        
    ]
    

})



export class ToDoListModule{

}