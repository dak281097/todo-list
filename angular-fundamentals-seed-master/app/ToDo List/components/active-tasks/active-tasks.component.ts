import {Component,Input} from '@angular/core';
import { Task } from '../../models/Todo-list.interface';


@Component({
    selector:"active-tasks",
    template:`
    <div>Tasks To Be Completed
    <div>
    <ul *ngFor='let task of detail'>
       
        <li  *ngIf='!task.completed'>{{task.task_name}}</li></ul>
    </div>
    </div>
    `
})
export class ActiveTasksComponent{
    @Input()
    detail:Task[];
}