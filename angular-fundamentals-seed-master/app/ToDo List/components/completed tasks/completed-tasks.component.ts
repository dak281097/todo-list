import {Component,Input} from '@angular/core';
import { Task } from '../../models/Todo-list.interface';


@Component({
    selector:"completed-tasks",
    template:`
    <div>Completed tasks
    <div>
    <ul *ngFor='let task of detail'>
       
        <li  *ngIf='task.completed'>{{task.task_name}}</li></ul>
    </div>
    </div>
    `
})
export class CompletedTasksComponent{
    @Input()
    detail:Task[];
}