import {Component,OnInit} from '@angular/core';
import { Task } from '../../models/Todo-list.interface';



@Component({
    selector: 'todo-list',
    styleUrls: ['ToDo-list.component.scss'],
    template: `
    <div class="todo-list">
      <h3>ToDo List</h3>
      <div>
      <input 
        type="text"
        placeholder="Name Of The Task"
        (keydown.enter)="onAdd(TaskName.value)"
        #TaskName>
        <button type="button"
        
        (click)="onAdd(TaskName.value)"
        >
        Add Task</button>
        <div>
        <button type="button" (click)="onAll()">All</button>
        <button type="button" (click)="onActive()">Active</button>
        <button type="button" (click)="onCompleted()">Completed</button>
        <p></p>
        <h4>Tasks List</h4>
        </div>
        <div *ngIf="choice==1">
        All Tasks
        <ul *ngFor="let task of tasks;let i=index;">
        <span class="status" [class.finshed]= 'task.completed' >
        <li>
        {{task.task_name}}
        
        <button 
        type="button" 
        *ngIf="!task.completed"
        (click)="task.completed=true">Finished</button>
        <button 
        type="button" (click)="onRemove(task)">Remove</button>
        </li>
        </span>
        </ul>
        </div>
        <div *ngIf="choice==2">
        <active-tasks 
      [detail]="tasks"></active-tasks>
        </div>
      <div *ngIf="choice==3"><completed-tasks 
      [detail]="tasks"></completed-tasks>
      </div>
    </div>
  `})

export class ToDoListComponent implements OnInit{

    tasks: Task[];

    choice:number=1;

    ngOnInit(){
        this.tasks=[
        {task_name: "first",completed:false},
        {task_name: "second",completed:false},

    ];}
    
    
    onAdd(name:string){
        this.tasks.push({task_name:name,completed:false});
       
    }

    onRemove(task:Task){
        let index=this.tasks.indexOf(task)
        this.tasks.splice(index,1);
    }
   
    onAll(){
      this.choice=1;
    }

    onActive(){
      this.choice=2;
    }
    onCompleted(){
      this.choice=3;
    }
   
}